# Machine Learning Engineer Nanodegree
## Capstone project: Breast Cancer Wisconsin (Diagnostic) Data Set

This project aims to find binary classifier that will be able to predict character of cells (malign/benign) based on Breast Cancer Wisconsin (Diagnostic) Data Set.

Dependencies
------------
```python
scikit-learn
seaborn  
bokeh  
prettyprint  
```

Dataset
-------

Dataset can be obtained from websites:  
[UCI MLR](https://archive.ics.uci.edu/ml/datasets/Breast+Cancer+Wisconsin+%28Diagnostic%29)  
[Kaggle](https://www.kaggle.com/uciml/breast-cancer-wisconsin-data)

Or direclty using sklearn package:  

```python
from sklearn.datasets import load_breast_cancer
data = load_breast_cancer()
```
